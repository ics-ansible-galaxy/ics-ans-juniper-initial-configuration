import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('juniper_initial_configuration')


def test_config_file_exists(host):
    assert not host.file('/cfg/sw-cslab-prtest-01.cslab.esss.lu.se.cfg').exists
    assert host.file('/cfg/sw-cslab-prtest-02.cslab.esss.lu.se.cfg').exists
    assert host.file('/cfg/switch1.cfg').exists
